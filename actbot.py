# -*- coding: utf-8 -*-
import sys sys.path.append('/home/dima/compat') import urllib import re import json import time import query import wikipedia as pywikibot wiki = 'ru' 
black_users = {'ro':['MessengerBot']} class DimaStBkBot(object):
	
	def __init__(self, delay):
		self.delay = delay
		self.site = pywikibot.getSite(wiki)
		self.page = pywikibot.Page(self.site, u'User:Dima st bk bot/Самые активные боты')
		self.bots = self.getlistbot()
		pywikibot.output(u'Bots:\03{lightgreen}' + str(len(self.bots['query']['allusers'])) + '\03{default}')
		self.text = u'{{Автонумерация\r\n'
		self.text += u' | Столбцов = 9\r\n'
		self.text += u' | Название = ' + time.asctime() + ' by {{user|dima_st_bk_bot}}\r\n'
		self.text += u' | Оформление = standard sortable\r\n'
		self.text += u' | Заголовок1 = №\r\n'
		self.text += u' | Заголовок2 = Участник\r\n'
		self.text += u' | Заголовок3 = Правок в статьях\r\n' #0
		self.text += u' | Заголовок4 = Правок в шаблонах\r\n' #10
		self.text += u' | Заголовок5 = Правок в категориях\r\n' #14
		self.text += u' | Заголовок6 = Правок в файлах\r\n' #6
		self.text += u' | Заголовок7 = Правок в пространстве ВП\r\n' #4
		self.text += u' | Заголовок8 = Правок всего\r\n'
		self.text += u' | Заголовок9 = Процент правок в статьях\r\n'
		self.text += u' | Выравнивание3 = right\r\n'
		self.text += u' | Выравнивание4 = right\r\n'
		self.text += u' | Выравнивание5 = right\r\n'
		self.text += u' | Выравнивание6 = right\r\n'
		self.text += u' | Выравнивание7 = right\r\n'
		self.text += u' | Выравнивание8 = right\r\n'
		self.text += u' | Выравнивание9 = right\r\n'
		self.text += u' | Сортировка = 3#\r\n'
		
	def getlistbot(self):
		params = {
		'action' :'query',
		'list' :'allusers',
		'augroup' :'bot',
		'aulimit' :'500',
	#	'auprop' :'groups',
		}    
		
		return query.GetData(params, self.site)
		
	def getedit(self, bot):
		bn = {'0':'0', '4':'0', '6':'0', '10':'0', '14':'0' }
		exp = re.compile(ur'(?is)Total edits \(including deleted\): (.*?)<')
		space = re.compile(ur'(?is)legend: \[(.*?)\]')
		url = "http://tools.wmflabs.org/supercount/api.php?user=" + bot + "&project=" + wiki + ".wikipedia&format=json"
		try:
			f = urllib.urlopen(url.encode('utf-8'))
			s = f.read()
			f.close()
			js = json.loads(s)
		except:
			pywikibot.output(u'\03{lightred}HTTP ERROR\03{default}')
			return False
		try:
			if js.has_key('namespace_totals'):
				for j in js['namespace_totals']:
					if bn.has_key(j):
						bn[j] = js['namespace_totals'][j]
			perc = float(int(bn['0']))/int(js['totaledits'])*100
			return '||{{user|%s}}|%s|%s|%s|%s|%s|%s|%d%%\r\n' % (bot, bn['0'], bn['10'], bn['14'], bn['6'], bn['4'], js['totaledits'], perc)
		except:
			pywikibot.output(u'\03{lightred}PARSE ERROR\03{default}')
			return False
	
	def run(self):
		for bot in self.bots['query']['allusers']:
			if wiki in black_users:
				if bot['name'] in black_users[wiki]:
					pywikibot.output(u'\03{lightyellow}' + bot['name'] + '\03{default}')
					continue
			pywikibot.output(u'\03{lightgreen}' + bot['name'] + '\03{default}')
			edit = False
			i = 0
			while edit == False and i < 10:
				edit = self.getedit(bot['name'])
				i += 1
			self.text += edit
			time.sleep(self.delay)
		print 'Готово.\nЗапись страницы..'
		self.text += '}}'
		self.page.put(self.text, u'Бот: обновление данных')
	
def main():
	pywikibot.output(u'Wiki:\03{lightgreen}' + wiki + '\03{default}')
	delay = 3
	for arg in pywikibot.handleArgs():
		if arg.startswith('-delay:'):
			delay = int(arg[7:])
		else:
			pywikibot.showHelp()
			return
	bot = DimaStBkBot(delay)
	try:
		bot.run()
	except KeyboardInterrupt:
		pywikibot.output('\nВыход...')
	
if __name__ == "__main__":
	main()
